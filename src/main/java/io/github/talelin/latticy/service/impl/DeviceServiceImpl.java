package io.github.talelin.latticy.service.impl;

import io.github.talelin.latticy.common.util.DateUtil;
import io.github.talelin.latticy.dto.book.CreateOrUpdateBookDTO;
import io.github.talelin.latticy.dto.device.CreateOrUpdateDeviceDTO;
import io.github.talelin.latticy.mapper.DeviceMapper;
import io.github.talelin.latticy.model.*;
import io.github.talelin.latticy.service.DeviceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 设备接口实现类
 */
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceMapper mDeviceMapper;

    @Override
    public List<DeviceDO> getAllDeviceByOnline(Integer online) {
        return mDeviceMapper.getDeviceByOnline(online);
    }

    @Override
    public List<DeviceDO> getAllDeviceByTaskStatus(Integer taskStatus) {
        return mDeviceMapper.getDeviceByTaskStatus(taskStatus);
    }

    @Override
    public DeviceDO getById(Integer id) {
        return mDeviceMapper.selectById(id);
    }

    @Override
    public DeviceDO getByDeviceId(String deviceId) {
        return mDeviceMapper.getByDeviceId(deviceId);
    }

    @Override
    public boolean updateDevice(DeviceDO device) {
        device.setUpdateTime(new Date());
        return mDeviceMapper.updateById(device)>0;
    }

    @Override
    public boolean createDevice(DeviceDO device) {
        device.setOnline(1);
        device.setTaskStatus(0);
        device.setVersion(1);
        device.setSendSuccessCount(0);
        device.setUpdateTime(new Date());
        return mDeviceMapper.insert(device) > 0;
    }

    @Override
    public List<DeviceDO> findAll() {
        return mDeviceMapper.selectList(null);
    }

    @Override
    public boolean deleteById(Integer id) {
        return mDeviceMapper.deleteById(id) > 0;
    }

    @Override
    public List<DeviceDO> getWorkingDevice(){
        return mDeviceMapper.getWorkingDevice();
    }

    public List<Device> convertFromDO(List<DeviceDO> deviceList) {
        List<Device> devices = new ArrayList<>();
        deviceList.forEach(model -> {
            Date updateTime = model.getUpdateTime();
            Device device = new Device();
            BeanUtils.copyProperties(model, device);
            if (updateTime != null) {
                device.setTime(DateUtil.formatDatetime(model.getUpdateTime()));
            }
            devices.add(device);
        });
        return devices;
    }

    @Override
    public boolean updateSucNum(List<DeviceDO> deviceList) {
        int count=0;
        for (DeviceDO device: deviceList) {
            device.setSendSuccessCount(0);
            if(mDeviceMapper.updateById(device)>0){
                count++;
            }
        }
        if(deviceList.size() == count){
            return true;
        }else {
            return false;
        }
    }
}
