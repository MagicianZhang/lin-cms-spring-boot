package io.github.talelin.latticy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.github.talelin.latticy.common.mybatis.LinPage;
import io.github.talelin.latticy.common.util.BeanCopyUtil;
import io.github.talelin.latticy.common.util.DateUtil;
import io.github.talelin.latticy.dto.receive.CreateOrUpdateReceiveDTO;
import io.github.talelin.latticy.mapper.InsSendUserInfoMapper;
import io.github.talelin.latticy.model.InsAccountInfoDO;
import io.github.talelin.latticy.model.InsSendUserInfoDO;
import io.github.talelin.latticy.model.LogDO;
import io.github.talelin.latticy.model.Receive;
import io.github.talelin.latticy.service.InsSendUserInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InsSendUserInfoServiceImpl implements InsSendUserInfoService {

    @Autowired
    private InsSendUserInfoMapper mInsSendUserInfoMapper;

    @Override
    public boolean createInsAccount(CreateOrUpdateReceiveDTO validator) {
        InsSendUserInfoDO receive = new InsSendUserInfoDO();
        receive.setUsername(validator.getUsername());
        receive.setStatus(0);
        return mInsSendUserInfoMapper.insert(receive) > 0;
    }

    @Override
    public boolean createReceive(InsSendUserInfoDO insSendUserInfo) {
        insSendUserInfo.setStatus(0);
        return mInsSendUserInfoMapper.insert(insSendUserInfo) > 0;
    }

    @Override
    public boolean updateInsAccount(InsSendUserInfoDO insSendUserInfo, CreateOrUpdateReceiveDTO validator) {
        insSendUserInfo.setUsername(validator.getUsername());
        insSendUserInfo.setUpdateTime(new Date());
        return mInsSendUserInfoMapper.updateById(insSendUserInfo)>0;
    }

    @Override
    public boolean updateInsAccountStatus(InsSendUserInfoDO insSendUserInfo) {
        insSendUserInfo.setUpdateTime(new Date());
        return mInsSendUserInfoMapper.updateById(insSendUserInfo)>0;
    }

    @Override
    public List<InsSendUserInfoDO> getListByStatus(Integer status) {
        return mInsSendUserInfoMapper.findAllByStatus(status);
    }


    @Override
    public List<Receive> findAll() {
        List<InsSendUserInfoDO> insSendUserList = mInsSendUserInfoMapper.findAll();
        List<Receive> receiveList = new ArrayList<>();
        for (InsSendUserInfoDO model :insSendUserList) {
            Receive receive = new Receive();
            receive.setId(model.getId());
            receive.setUsername(model.getUsername());
            receive.setStatus(model.getStatus());
            receive.setTime(DateUtil.formatToStr(model.getUpdateTime().getTime()));
            receiveList.add(receive);
        }
        return receiveList;
    }

    @Override
    public IPage<InsSendUserInfoDO> getReceivePage(Integer count, Integer page) {
        LinPage<InsSendUserInfoDO> pager = new LinPage<>(page, count);
        IPage<InsSendUserInfoDO> paging = mInsSendUserInfoMapper.getReceivePage(pager);
        return paging;
    }

    @Override
    public InsSendUserInfoDO selectByUsername(String username) {
        return mInsSendUserInfoMapper.selectByUsername(username);
    }

    @Override
    public boolean isExistReceive(String username) {
        return mInsSendUserInfoMapper.selectCountByUsername(username) > 0;
    }

    @Override
    public InsSendUserInfoDO getById(Integer id) {
        return mInsSendUserInfoMapper.selectById(id);
    }

    @Override
    public boolean deleteById(Integer id) {
        return mInsSendUserInfoMapper.deleteById(id) > 0;
    }

    @Override
    public int selectCountByStatus(Integer status) {
        return mInsSendUserInfoMapper.selectCountByStatus(status);
    }

    public List<Receive> convertFromDO(List<InsSendUserInfoDO> sendUserInfos) {
        List<Receive> receives = new ArrayList<>();
        sendUserInfos.forEach(model -> {
            Date updateTime = model.getUpdateTime();
            Receive receive = new Receive();
            BeanUtils.copyProperties(model, receive);
            if (updateTime != null) {
                receive.setTime(DateUtil.formatToStr(model.getUpdateTime().getTime()));
            }
            receives.add(receive);
        });
        return receives;
    }

    @Override
    public IPage<InsSendUserInfoDO> search(Integer count, Integer page, Integer status) {
        LinPage<InsSendUserInfoDO> pager = new LinPage<>(page, count);
        IPage<InsSendUserInfoDO> paging = mInsSendUserInfoMapper.searchByStatus(pager,status);
        return paging;
    }

    @Override
    public boolean deleteAll(Integer status) {
        QueryWrapper<InsSendUserInfoDO> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(InsSendUserInfoDO::getStatus, status);
        return  mInsSendUserInfoMapper.delete(wrapper)>0;
    }

}
