package io.github.talelin.latticy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.github.talelin.latticy.common.mybatis.LinPage;
import io.github.talelin.latticy.common.util.DateUtil;
import io.github.talelin.latticy.dto.account.CreateOrUpdateAccountDTO;
import io.github.talelin.latticy.mapper.InsAccountInfoMapper;
import io.github.talelin.latticy.model.Account;
import io.github.talelin.latticy.model.InsAccountInfoDO;
import io.github.talelin.latticy.service.InsAccountInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InsAccountInfoServiceImpl implements InsAccountInfoService {

    @Autowired
    private InsAccountInfoMapper mInsAccountInfoMapper;

    @Override
    public boolean createInsAccount(CreateOrUpdateAccountDTO validator) {
        InsAccountInfoDO insAccount = new InsAccountInfoDO();
        insAccount.setUsername(validator.getUsername());
        insAccount.setPassword(validator.getPassword());
        insAccount.setStatus(InsAccountInfoDO.STATUS_NORMAL);
        insAccount.setCount(0);
        insAccount.setUpdateTime(new Date());
        return mInsAccountInfoMapper.insert(insAccount) > 0;
    }

    @Override
    public boolean createInsAccount(InsAccountInfoDO accountInfo) {
        accountInfo.setStatus(InsAccountInfoDO.STATUS_NORMAL);
        accountInfo.setCount(0);
        accountInfo.setUpdateTime(new Date());
        return mInsAccountInfoMapper.insert(accountInfo) > 0;
    }

    @Override
    public boolean updateInsAccount(InsAccountInfoDO insAccountInfo, CreateOrUpdateAccountDTO validator) {
        insAccountInfo.setUsername(validator.getUsername());
        insAccountInfo.setPassword(validator.getPassword());
        //修改密码变为正常状态
        insAccountInfo.setStatus(InsAccountInfoDO.STATUS_NORMAL);
        insAccountInfo.setUpdateTime(new Date());
        return mInsAccountInfoMapper.updateById(insAccountInfo) > 0;
    }

    @Override
    public boolean updateInsAccountInfo(InsAccountInfoDO insAccountInfo) {
        insAccountInfo.setUpdateTime(new Date());
        return mInsAccountInfoMapper.updateById(insAccountInfo) > 0;
    }

    @Override
    public List<InsAccountInfoDO> findAll() {
        return mInsAccountInfoMapper.selectList(null);
    }

    @Override
    public List<InsAccountInfoDO> selectAllTask() {
        return mInsAccountInfoMapper.selectAllTask();
    }

    @Override
    public List<InsAccountInfoDO> getAllListByNoTask() {
        return mInsAccountInfoMapper.getAllListByNoTask();
    }

    public List<InsAccountInfoDO> selectAllTaskByDeviceId(String deviceId) {
        return mInsAccountInfoMapper.selectAllTaskByDeviceId(deviceId);
    }

    @Override
    public boolean isExistInsAccount(String username) {
        return mInsAccountInfoMapper.selectCountByUsername(username) > 0;
    }

    @Override
    public InsAccountInfoDO getById(Integer id) {
        return mInsAccountInfoMapper.selectById(id);
    }

    @Override
    public InsAccountInfoDO selectByUsername(String username) {
        return mInsAccountInfoMapper.selectByUsername(username);
    }

    @Override
    public boolean deleteById(Integer id) {
        return mInsAccountInfoMapper.deleteById(id)>0;
    }

    @Override
    public List<InsAccountInfoDO> findAllByStatus(Integer status) {
        return mInsAccountInfoMapper.findAllByStatus(status);
    }

    @Override
    public IPage<InsAccountInfoDO> getAccountPage(Integer count, Integer page) {
        LinPage<InsAccountInfoDO> pager = new LinPage<>(page, count);
        IPage<InsAccountInfoDO> paging = mInsAccountInfoMapper.getAccountPage(pager);
        return paging;
    }

    @Override
    public IPage<InsAccountInfoDO> search(Integer count, Integer page, Integer status) {
        LinPage<InsAccountInfoDO> pager = new LinPage<>(page, count);
        IPage<InsAccountInfoDO> paging = mInsAccountInfoMapper.searchByStatus(pager,status);
        return paging;
    }

    public List<InsAccountInfoDO> selectByDeviceId(String deviceId) {
        Map columnMap = new HashMap();
        columnMap.put("lastDeviceId",deviceId);
        return mInsAccountInfoMapper.selectByMap(columnMap);
    }

    public List<Account> convertFromDO(List<InsAccountInfoDO> sendUserInfos) {
        List<Account> accounts = new ArrayList<>();
        sendUserInfos.forEach(model -> {
            Date updateTime = model.getUpdateTime();
            Account account = new Account();
            BeanUtils.copyProperties(model, account);
            if (updateTime != null) {
                account.setTime(DateUtil.formatToStr(model.getUpdateTime().getTime()));
            }
            accounts.add(account);
        });
        return accounts;
    }

    @Override
    public boolean deleteAllByStatus(int status) {
        QueryWrapper<InsAccountInfoDO> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(InsAccountInfoDO::getStatus, status);
        return  mInsAccountInfoMapper.delete(wrapper)>0;
    }

    @Override
    public int selectCountByStatus(Integer status) {
        return mInsAccountInfoMapper.selectCountByStatus(status);
    }
}
