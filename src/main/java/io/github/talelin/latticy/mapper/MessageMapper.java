package io.github.talelin.latticy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.github.talelin.latticy.model.MessageDO;
import io.github.talelin.latticy.model.PermissionDO;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 发送信息Mapper
 */
@Repository
public interface MessageMapper extends BaseMapper<MessageDO> {
    
    List<MessageDO> selectMessageByTag(@Param("msgTag") String msgTag);
}
