package io.github.talelin.latticy.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.github.talelin.latticy.common.mybatis.LinPage;
import io.github.talelin.latticy.model.InsSendUserInfoDO;
import io.github.talelin.latticy.model.LogDO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 发送信息对象Mapper
 */
@Repository
public interface InsSendUserInfoMapper extends BaseMapper<InsSendUserInfoDO> {

    /**
     * 根据用户名查询发送对象是否存在
     * @param username
     * @return
     */
    int selectCountByUsername(String username);

    /**
     * 根据用户名发送对象信息
     * @return
     */
    InsSendUserInfoDO selectByUsername(String username);

    int selectCountByStatus(Integer status);

    List<InsSendUserInfoDO> findAll();

    IPage<InsSendUserInfoDO> getReceivePage(LinPage<InsSendUserInfoDO> pager);

    List<InsSendUserInfoDO> findAllByStatus(Integer status);

    IPage<InsSendUserInfoDO> searchByStatus(LinPage<InsSendUserInfoDO> pager, Integer status);
}
