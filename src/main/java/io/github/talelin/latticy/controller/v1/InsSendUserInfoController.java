package io.github.talelin.latticy.controller.v1;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.github.talelin.autoconfigure.exception.NotFoundException;
import io.github.talelin.core.annotation.GroupRequired;
import io.github.talelin.core.annotation.LoginRequired;
import io.github.talelin.core.annotation.PermissionMeta;
import io.github.talelin.latticy.common.util.PageUtil;
import io.github.talelin.latticy.dto.receive.CreateOrUpdateReceiveDTO;
import io.github.talelin.latticy.model.InsSendUserInfoDO;
import io.github.talelin.latticy.model.Receive;
import io.github.talelin.latticy.service.InsSendUserInfoService;
import io.github.talelin.latticy.vo.CreatedVO;
import io.github.talelin.latticy.vo.DeletedVO;
import io.github.talelin.latticy.vo.PageResponseVO;
import io.github.talelin.latticy.vo.UpdatedVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.util.*;

/**
 * 发送对象控制器
 */
@RestController
@RequestMapping("/v1/receive")
@Validated
public class InsSendUserInfoController {

    @Autowired
    private InsSendUserInfoService insSendUserInfoService;


    @GetMapping("/{id}")
    public InsSendUserInfoDO getReceive(@PathVariable(value = "id") @Positive(message = "{id.positive}") Integer id) {
        InsSendUserInfoDO receive = insSendUserInfoService.getById(id);
        if (receive == null) {
            throw new NotFoundException(10302);
        }
        return receive;
    }

    /**
     * 查询所有账号信息
     * @return
     */
    @GetMapping("/page")
    public PageResponseVO<Receive> getReceives(@RequestParam(name = "count", required = false, defaultValue = "10")
                                                   @Min(value = 1, message = "{page.count.min}")
                                                   @Max(value = 30, message = "{page.count.max}") Integer count,
                                               @RequestParam(name = "page", required = false, defaultValue = "0")
                                                   @Min(value = 0, message = "{page.number.min}") Integer page) {
        IPage<InsSendUserInfoDO> iPage = insSendUserInfoService.getReceivePage(count, page);
        List<Receive> receiveList = insSendUserInfoService.convertFromDO(iPage.getRecords());
        return PageUtil.build(iPage, receiveList);
    }

    @GetMapping("/search")
    @LoginRequired
    public PageResponseVO<Receive> search(
            @RequestParam(name = "count", required = false, defaultValue = "10")
            @Min(value = 1, message = "{page.count.min}")
            @Max(value = 30, message = "{page.count.max}") Integer count,
            @RequestParam(name = "page", required = false, defaultValue = "0")
            @Min(value = 0, message = "{page.number.min}") Integer page,
            @RequestParam(name = "status", required = false, defaultValue = "0") Integer status
    ) {
        IPage<InsSendUserInfoDO> iPage = insSendUserInfoService.search(count, page,status);
        List<Receive> receiveList = insSendUserInfoService.convertFromDO(iPage.getRecords());
        return PageUtil.build(iPage, receiveList);
    }

    /**
     * 查询所有状态数量
     * @return
     */
    @GetMapping("/count")
    public Map getStatusCount() {
        Map hashMap = new HashMap<>();
        hashMap.put("defNum",insSendUserInfoService.selectCountByStatus(0));
        hashMap.put("preNum",insSendUserInfoService.selectCountByStatus(1));
        hashMap.put("sucNum",insSendUserInfoService.selectCountByStatus(2));
        hashMap.put("invalidNum",insSendUserInfoService.selectCountByStatus(4));
        return hashMap;
    }

    @PostMapping("")
    public CreatedVO createReceive(@RequestBody @Validated CreateOrUpdateReceiveDTO validator) {
        if(insSendUserInfoService.isExistReceive(validator.getUsername())){
            throw new NotFoundException(10304);
        }else{
            insSendUserInfoService.createInsAccount(validator);
        }
        return new CreatedVO(20);
    }


    @PutMapping("/{id}")
    public UpdatedVO updateReceive(@PathVariable("id") @Positive(message = "{id.positive}") Integer id, @RequestBody @Validated CreateOrUpdateReceiveDTO validator) {
        InsSendUserInfoDO receive = insSendUserInfoService.getById(id);
        if (receive == null) {
            throw new NotFoundException(10302);
        }
        insSendUserInfoService.updateInsAccount(receive, validator);
        return new UpdatedVO(21);
    }

    /**
     * 删除账号
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @GroupRequired
    @PermissionMeta(value = "删除账号", module = "账号")
    public DeletedVO deleteReceive(@PathVariable("id") @Positive(message = "{id.positive}") Integer id) {
        InsSendUserInfoDO receive = insSendUserInfoService.getById(id);
        if (receive == null) {
            throw new NotFoundException(10302);
        }
        insSendUserInfoService.deleteById(receive.getId());
        return new DeletedVO(22);
    }

    /**
     * 删除成功账号
     * @return
     */
    @DeleteMapping("/delete/suc")
    @PermissionMeta(value = "删除所有发送成功账号", module = "账号")
    public DeletedVO deleteSuccess() {

        if(insSendUserInfoService.deleteAll(2)){
            return new DeletedVO(22,"删除成功");
        }else{
            return new DeletedVO(10404,"删除失败");
        }
    }

    /**
     * 删除异常账号
     * @return
     */
    @DeleteMapping("/delete/invalid")
    @PermissionMeta(value = "删除所有发送异常账号", module = "账号")
    public DeletedVO deleteInvalid() {

        if(insSendUserInfoService.deleteAll(4)){
            return new DeletedVO(22,"删除成功");
        }else{
            return new DeletedVO(10404,"删除失败");
        }
    }
}
