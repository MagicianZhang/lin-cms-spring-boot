package io.github.talelin.latticy.controller.v1;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.github.talelin.autoconfigure.exception.NotFoundException;
import io.github.talelin.core.annotation.GroupRequired;
import io.github.talelin.core.annotation.LoginRequired;
import io.github.talelin.core.annotation.PermissionMeta;
import io.github.talelin.latticy.common.util.PageUtil;
import io.github.talelin.latticy.dto.account.CreateOrUpdateAccountDTO;
import io.github.talelin.latticy.model.*;
import io.github.talelin.latticy.service.InsAccountInfoService;
import io.github.talelin.latticy.vo.CreatedVO;
import io.github.talelin.latticy.vo.DeletedVO;
import io.github.talelin.latticy.vo.PageResponseVO;
import io.github.talelin.latticy.vo.UpdatedVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ins账号控制器
 */
@RestController
@RequestMapping("/v1/account")
@Validated
public class InsAccountController {

    @Autowired
    private InsAccountInfoService insAccountInfoService;

    @GetMapping("/{id}")
    public InsAccountInfoDO getInsAccount(@PathVariable(value = "id") @Positive(message = "{id.positive}") Integer id) {
        InsAccountInfoDO insAccount = insAccountInfoService.getById(id);
        if (insAccount == null) {
            throw new NotFoundException(10302);
        }
        return insAccount;
    }


    /**
     * 查询所有状态数量
     * @return
     */
    @GetMapping("/count")
    public Map getStatusCount() {
        Map hashMap = new HashMap<>();
        hashMap.put("defNum",insAccountInfoService.selectCountByStatus(0));
        hashMap.put("pwErrorNum",insAccountInfoService.selectCountByStatus(1));
        hashMap.put("frozenNum",insAccountInfoService.selectCountByStatus(2));
        hashMap.put("doingNum",insAccountInfoService.selectCountByStatus(100));
        hashMap.put("exNum",insAccountInfoService.selectCountByStatus(101));
        return hashMap;
    }

    /**
     * 查询所有账号信息
     * @return
     */
    @GetMapping("/page")
    public PageResponseVO<Account> getInsAccounts(@RequestParam(name = "count", required = false, defaultValue = "10")
                                               @Min(value = 1, message = "{page.count.min}")
                                               @Max(value = 30, message = "{page.count.max}") Integer count,
                                               @RequestParam(name = "page", required = false, defaultValue = "0")
                                               @Min(value = 0, message = "{page.number.min}") Integer page) {
        IPage<InsAccountInfoDO> iPage = insAccountInfoService.getAccountPage(count, page);
        List<Account> accountList = insAccountInfoService.convertFromDO(iPage.getRecords());
        return PageUtil.build(iPage, accountList);
    }

    @GetMapping("/search")
    @LoginRequired
    public PageResponseVO<InsAccountInfoDO> search(
            @RequestParam(name = "count", required = false, defaultValue = "10")
            @Min(value = 1, message = "{page.count.min}")
            @Max(value = 30, message = "{page.count.max}") Integer count,
            @RequestParam(name = "page", required = false, defaultValue = "0")
            @Min(value = 0, message = "{page.number.min}") Integer page,
            @RequestParam(name = "status", required = false, defaultValue = "0") Integer status
    ) {
        IPage<InsAccountInfoDO> iPage = insAccountInfoService.search(count, page,status);
        return PageUtil.build(iPage, iPage.getRecords());
    }


    @PostMapping("")
    public CreatedVO createInsAccount(@RequestBody @Validated CreateOrUpdateAccountDTO validator) {
        if(insAccountInfoService.isExistInsAccount(validator.getUsername())){
            throw new NotFoundException(10304);
        }else{
            insAccountInfoService.createInsAccount(validator);
        }
        return new CreatedVO(20);
    }


    @PutMapping("/{id}")
    public UpdatedVO updateInsAccount(@PathVariable("id") @Positive(message = "{id.positive}") Integer id, @RequestBody @Validated CreateOrUpdateAccountDTO validator) {
        InsAccountInfoDO insAccount = insAccountInfoService.getById(id);
        if (insAccount == null) {
            throw new NotFoundException(10302);
        }
        insAccountInfoService.updateInsAccount(insAccount, validator);
        return new UpdatedVO(21);
    }

    /**
     * 重置账号信息
     * @param id
     * @return
     */
    @PutMapping("/reload/{id}")
    public UpdatedVO reloadAccountInfo(@PathVariable("id") @Positive(message = "{id.positive}") Integer id) {
        InsAccountInfoDO insAccount = insAccountInfoService.getById(id);
        if (insAccount == null) {
            throw new NotFoundException(10302);
        }
        insAccount.setStatus(InsAccountInfoDO.STATUS_NORMAL);
        insAccount.setLastDeviceId("");
        insAccount.setLastDeviceTag("");
        insAccountInfoService.updateInsAccountInfo(insAccount);
        return new UpdatedVO(21);
    }

    /**
     * 删除账号
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @GroupRequired
    @PermissionMeta(value = "删除账号", module = "账号")
    public DeletedVO deleteInsAccount(@PathVariable("id") @Positive(message = "{id.positive}") Integer id) {
        InsAccountInfoDO accountInfo = insAccountInfoService.getById(id);
        if (accountInfo == null) {
            throw new NotFoundException(10302);
        }
        insAccountInfoService.deleteById(accountInfo.getId());
        return new DeletedVO(22);
    }

    /**
     * 删除账号
     * @return
     */
    @DeleteMapping("/delete")
    @PermissionMeta(value = "删除所有冻结账号账号", module = "账号")
    public DeletedVO deleteInsAccount() {

        if(insAccountInfoService.deleteAllByStatus(2)){
            return new DeletedVO(22,"删除成功");
        }else{
            return new DeletedVO(10404,"删除失败");
        }
    }

    @DeleteMapping("/delete_ex")
    @PermissionMeta(value = "删除所有异常账号", module = "账号")
    public DeletedVO deleteInsAccountAll() {

        if(insAccountInfoService.deleteAllByStatus(101)){
            return new DeletedVO(22,"删除成功");
        }else{
            return new DeletedVO(10404,"删除失败");
        }
    }


}
