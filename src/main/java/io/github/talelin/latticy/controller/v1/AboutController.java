package io.github.talelin.latticy.controller.v1;

import io.github.talelin.latticy.model.AboutDO;
import io.github.talelin.latticy.model.DeviceDO;
import io.github.talelin.latticy.service.DeviceService;
import io.github.talelin.latticy.service.InsAccountInfoService;
import io.github.talelin.latticy.service.InsSendUserInfoService;
import io.github.talelin.latticy.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/about")
@Validated
@Slf4j
public class AboutController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private InsAccountInfoService insAccountInfoService;

    @Autowired
    private InsSendUserInfoService insSendUserInfoService;


    @GetMapping("")
    public AboutDO getDevices() {
        AboutDO about = new AboutDO();
        about.setDeviceNum(deviceService.findAll().size());
        about.setDeviceWorking(deviceService.getWorkingDevice().size());
        about.setDeviceOnline(deviceService.getAllDeviceByOnline(1).size());
        about.setDeviceOutline(deviceService.getAllDeviceByOnline(0).size());

        about.setReceiveNum(insSendUserInfoService.findAll().size());
        about.setReceiveVaild(insSendUserInfoService.getListByStatus(0).size());
        about.setReceiveSuc(insSendUserInfoService.getListByStatus(2).size());

        about.setAccountNum(insAccountInfoService.findAll().size());
        about.setAccountInvaild(insAccountInfoService.findAllByStatus(1).size()+insAccountInfoService.findAllByStatus(2).size());
        about.setAccountSuc(insAccountInfoService.findAllByStatus(3).size());
        about.setAccountVaild(insAccountInfoService.findAllByStatus(0).size());

        return about;
    }
}
