package io.github.talelin.latticy.model;

import lombok.Data;

@Data
public class Receive extends InsSendUserInfoDO{
    private String time;
}
