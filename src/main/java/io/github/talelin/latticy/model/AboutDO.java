package io.github.talelin.latticy.model;

import lombok.Data;

@Data
public class AboutDO {
    //设备
    private int deviceNum;
    private int deviceWorking;
    private int deviceOutline;
    private int deviceOnline;

    //接收对象
    private int receiveNum;
    private int receiveVaild;
    private int receiveSuc;

    //ins账号
    private int accountNum;
    private int accountVaild;
    private int accountInvaild;
    private int accountSuc;
}
