package io.github.talelin.latticy.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * ins账号列表
 * </p>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("ins_account_info")
@EqualsAndHashCode(callSuper = true)
public class InsAccountInfoDO extends BaseModel implements Serializable {

    private static final long serialVersionUID= -1388321833414589409L;

    public static int STATUS_NORMAL = 0;
    public static int STATUS_PWD_ERROR = 1;
    /**
     * 冻结
     */
    public static int STATUS_ERROR = 2;
    /**
     * 运行中
     */
    public static int STATUS_RUNNING = 100;
    /**
     * 账号异常
     */
    public static int STATUS_EXCEPTION = 101;
    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 最后一次使用的设备id
     */
    private String lastDeviceId;

    /**
     * ins账号：0正常 1密码错误 2 账号冻结,100 任务进行中，101 操作异常
     */
    private Integer status;


    /**
     * 发送次数
     */
    private Integer count;
    
    //设备标签
    private String lastDeviceTag;
}
