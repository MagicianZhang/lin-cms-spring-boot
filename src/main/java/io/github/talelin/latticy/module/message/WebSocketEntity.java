package io.github.talelin.latticy.module.message;

import org.springframework.web.socket.WebSocketSession;

/**
 * @author wrs 393895279@qq.com
 * @date 2022/12/7.
 * @desc
 */
public class WebSocketEntity {
    
    
    private long lastTime;
    
    protected int sendCount;
    
    public int getSendCount() {
        return sendCount;
    }
    
    public void setSendCount(int sendCount) {
        this.sendCount = sendCount;
    }

    
    public long getLastTime() {
        return lastTime;
    }
    
    public void setLastTime(long lastTime) {
        this.lastTime = lastTime;
    }
}
